#!/usr/bin/env python
""" ParametrisationSequence.

Chain multiple ParametrisationSchemes.
"""

from __future__ import absolute_import

import logging


__author__ = "Stefan Gadatsch"
__credits__ = ["Stefan Gadatsch", "Andrea Gabrielli"]
__version__ = "0.1"
__maintainer__ = "Stefan Gadatsch"
__email__ = "stefan.gadatsch@cern.ch"

__all__ = [
    'ParametrisationSequence'
]


FORMAT = '%(asctime)s - %(processName)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
existing_logger = logging.getLogger('x')


class ParametrisationSequence(object):
    def __init__(self, name, pois=None):
        if pois is None:
            pois = ""

        self._name = name
        self._pois = pois
        self._sequence = []

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def ParametersOfInterest(self):
        return self._pois

    @ParametersOfInterest.setter
    def ParametersOfInterest(self, pois):
        self._pois = pois

    @property
    def sequence(self):
        return self._sequence

    @sequence.setter
    def sequence(self, sequence):
        self._sequence = sequence

    def AddScheme(self, scheme):
        self._sequence.append(scheme)


if __name__ == '__main__':
    pass
