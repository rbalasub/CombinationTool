#!/usr/bin/env python

# ATLAS Higgs to WW* to lvlv combination (7 + 8 TeV)

# Load the shared library. To compile it, follow the instructions given in the
# README.
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem

try:
    gSystem.Load("lib/libCombinationTool.so")
except:
    print ("Could not load library. Make sure that it was compiled correctly.")

# Define
combined = ROOT.CombinedMeasurement("combined_master")

# Add measurement for lvlv 2011 to combined measurement
# The nominal snapshot to load initally is called "nominalNuis".
# The workspace "combined" is stored in the file "lvlv2011_125.5.root".
# "ModelConfig" holds all information about nuisance paramters, PDF, etc.
# The dataset to be added in the combination is called "obsData".
lvlv2011 = ROOT.Measurement("lvlv2011")
lvlv2011.SetSnapshotName("nominalNuis")
lvlv2011.SetFileName("workspaces/lvlv_7TeV_125.5.root")
lvlv2011.SetWorkspaceName("combined")
lvlv2011.SetModelConfigName("ModelConfig")
lvlv2011.SetDataName("obsData")
combined.AddMeasurement(lvlv2011)

# Add measurement for lvlv 2012 to combined measurement
# The nominal snapshot to load initally is called "nominalNuis".
# The workspace "combined" is stored in the file "lvlv2012_125.5.root".
# "ModelConfig" holds all information about nuisance paramters, PDF, etc.
# The dataset to be added in the combination is called "obsData".
lvlv2012 = ROOT.Measurement("lvlv2012")
lvlv2012.SetSnapshotName("nominalNuis")
lvlv2012.SetFileName("workspaces/lvlv_8TeV_125.5.root")
lvlv2012.SetWorkspaceName("combined")
lvlv2012.SetModelConfigName("ModelConfig")
lvlv2012.SetDataName("obsData")
combined.AddMeasurement(lvlv2012)

# Define a correlation scheme that should be used when combining the specified
# measurements. Parameters in the scheme that are not present in the
# measurements will be ignored.
correlation = ROOT.CorrelationScheme("CorrelationScheme")

# Define parameters of interest for the combined measurement. Only parameters
# present in the final workspace will be considered.
correlation.SetParametersOfInterest("mu,mu_XS7_ggF,mu_XS8_ggF,mu_XS7_VBF,mu_XS8_VBF,mu_XS7_WH,mu_XS8_WH,mu_XS7_ZH,mu_XS8_ZH,mu_BR_WW,mu_BR_tautau")

# Universal signal strength, not needed anymore as re-parametrisation and correction
# for tautau contamination takes care of it.
correlation.CorrelateParameter("lvlv2012::SigXsecOverSM_HWW,lvlv2012::SigXsecOverSM_Htt,lvlv2012::SigXsecOverSM,lvlv2011::SigXsecOverSM_HWW", "dummy[1.0]")

# Signal strengths for different production modes. Use a placeholder for the
# ditau signal strength as it needs to be corrected for using 125.5 GeV.
# Signal strength parameters are unconstrained by default.

# ggF
correlation.RenameParameter("lvlv2011", "ATLAS_sampleNorm_ggf125.5", "mu_XS7_ggF[1.0]")
correlation.RenameParameter("lvlv2012", "ATLAS_sampleNorm_ggf125.5", "mu_XS8_ggF[1.0]")
correlation.RenameParameter("lvlv2012", "ATLAS_sampleNorm_ggftt125", "mu_XS8_ggF_linear[1.0]")
# VBF
correlation.RenameParameter("lvlv2011", "ATLAS_sampleNorm_vbf125.5", "mu_XS7_VBF[1.0]")
correlation.RenameParameter("lvlv2012", "ATLAS_sampleNorm_vbf125.5", "mu_XS8_VBF[1.0]")
correlation.RenameParameter("lvlv2012", "ATLAS_sampleNorm_vbftt125", "mu_XS8_VBF_linear[1.0]")
# WH
correlation.RenameParameter("lvlv2011", "ATLAS_sampleNorm_wh125.5", "mu_XS7_WH[1.0]")
correlation.RenameParameter("lvlv2012", "ATLAS_sampleNorm_wh125.5", "mu_XS8_WH[1.0]")
# ZH
correlation.RenameParameter("lvlv2011", "ATLAS_sampleNorm_zh125.5", "mu_XS7_ZH[1.0]")
correlation.RenameParameter("lvlv2012", "ATLAS_sampleNorm_zh125.5", "mu_XS8_ZH[1.0]")

# Branching ratios into different final states
correlation.RenameParameter("lvlv2011", "mu_BR_WW,lvlv2012::mu_BR_WW", "mu_BR_WW[1.0]")
correlation.RenameParameter("lvlv2012", "mu_BR_tautau", "mu_BR_tautau_linear[1.0]")

# Parameters from lvlv 2011 that are just renamed, but not correlated
# to any other parameter in the combined workspace. Parameters starting with "scale_"
# are unconstrained scale factors. Other parameters typically have
# a Gaussian constraint
correlation.RenameParameter("lvlv2011", "alpha_ATLAS_JES_2011_Eta_TotalStat", "ATLAS_JES_2011_Eta_TotalStat")
correlation.RenameParameter("lvlv2011", "alpha_ATLAS_JES_2011_Statistical1", "ATLAS_JES_2011_Statistical1")
correlation.RenameParameter("lvlv2011", "alpha_ATLAS_JES_NonClosure_MC11c", "ATLAS_JES_NonClosure_MC11c")
correlation.RenameParameter("lvlv2011", "alpha_ATLAS_MET_RESOSOFT", "ATLAS_MET_RESOSOFT_HWW_2011")
correlation.RenameParameter("lvlv2011", "alpha_ATLAS_MET_SCALESOFT", "ATLAS_MET_SCALESOFT_HWW_2011")
correlation.RenameParameter("lvlv2011", "alpha_LUMI_2011", "ATLAS_LUMI_2011")
correlation.RenameParameter("lvlv2011", "ATLAS_norm_SF_MU_DY0j", "scale_ATLAS_norm_SF_MU_DY0j_lvlv2011")
correlation.RenameParameter("lvlv2011", "ATLAS_norm_SF_MU_DY1j", "scale_ATLAS_norm_SF_MU_DY1j_lvlv2011")
correlation.RenameParameter("lvlv2011", "ATLAS_norm_SF_MUSR_DY0j", "scale_ATLAS_norm_SF_MUSR_DY0j_lvlv2011")
correlation.RenameParameter("lvlv2011", "ATLAS_norm_SF_MUSR_DY1j", "scale_ATLAS_norm_SF_MUSR_DY1j_lvlv2011")
correlation.RenameParameter("lvlv2011", "ATLAS_norm_Top1j", "scale_ATLAS_norm_Top1j_lvlv2011")
correlation.RenameParameter("lvlv2011", "ATLAS_norm_Top2j", "scale_ATLAS_norm_Top2j_lvlv2011")
correlation.RenameParameter("lvlv2011", "ATLAS_norm_WW0j", "scale_ATLAS_norm_WW0j_lvlv2011")
correlation.RenameParameter("lvlv2011", "ATLAS_norm_WW1j", "scale_ATLAS_norm_WW1j_lvlv2011")
correlation.RenameParameter("lvlv2011", "PM_EFF_f_recoil_DY0j", "scale_PM_EFF_f_recoil_DY0j_lvlv2011")
correlation.RenameParameter("lvlv2011", "PM_EFF_f_recoil_DY1j", "scale_PM_EFF_f_recoil_DY1j_lvlv2011")
correlation.RenameParameter("lvlv2011", "PM_EFF_f_recoil_NDY_SR0j", "scale_PM_EFF_f_recoil_NDY_SR0j_lvlv2011")
correlation.RenameParameter("lvlv2011", "PM_EFF_f_recoil_NDY_SR1j", "scale_PM_EFF_f_recoil_NDY_SR1j_lvlv2011")
correlation.RenameParameter("lvlv2011", "PM_EFF_f_recoil_NDY_ZP0j", "scale_PM_EFF_f_recoil_NDY_ZP0j_lvlv2011")
correlation.RenameParameter("lvlv2011", "PM_EFF_f_recoil_NDY_ZP1j", "scale_PM_EFF_f_recoil_NDY_ZP1j_lvlv2011")

# Parameters from lvlv 2012 that are just renamed, but not correlated
# to any other parameter in the combined workspace. Parameters starting with "scale_"
# are unconstrained scale factors. Other parameters typically have
# a Gaussian constraint
correlation.RenameParameter("lvlv2012", "alpha_ATLAS_BTag_CEFF", "ATLAS_BTag_CEFF_2012")
correlation.RenameParameter("lvlv2012", "alpha_ATLAS_JES_2012_Eta_StatMethod", "ATLAS_JES_2012_Eta_StatMethod")
correlation.RenameParameter("lvlv2012", "alpha_ATLAS_JES_2012_PilePt", "ATLAS_JES_2012_PilePt")
correlation.RenameParameter("lvlv2012", "alpha_ATLAS_JES_2012_PileRho_HWW", "ATLAS_JES_2012_PileRho_HWW")
correlation.RenameParameter("lvlv2012", "alpha_ATLAS_MET_RESOSOFT", "ATLAS_MET_RESOSOFT_HWW_2012")
correlation.RenameParameter("lvlv2012", "alpha_ATLAS_MET_SCALESOFT", "ATLAS_MET_SCALESOFT_HWW_2012")
correlation.RenameParameter("lvlv2012", "alpha_ATLAS_MU_RESCALE_lvlv_2012", "ATLAS_MU_RESCALE_HWW_2012")
correlation.RenameParameter("lvlv2012", "alpha_ATLAS_TRACKMET_RESOSOFT", "ATLAS_TRACKMET_RESOSOFT")
correlation.RenameParameter("lvlv2012", "alpha_ATLAS_TRACKMET_SCALESOFT", "ATLAS_TRACKMET_SCALESOFT")
correlation.RenameParameter("lvlv2012", "alpha_LUMI_2012", "ATLAS_LUMI_2012_diboson")
correlation.RenameParameter("lvlv2012", "ATLAS_norm_SF_MU_DY0j", "scale_ATLAS_norm_SF_MU_DY0j_lvlv2012")
correlation.RenameParameter("lvlv2012", "ATLAS_norm_SF_MU_DY1j", "scale_ATLAS_norm_SF_MU_DY1j_lvlv2012")
correlation.RenameParameter("lvlv2012", "ATLAS_norm_SF_MUSR_DY0j", "scale_ATLAS_norm_SF_MUSR_DY0j_lvlv2012")
correlation.RenameParameter("lvlv2012", "ATLAS_norm_SF_MUSR_DY1j", "scale_ATLAS_norm_SF_MUSR_DY1j_lvlv2012")
correlation.RenameParameter("lvlv2012", "ATLAS_norm_Top1j", "scale_ATLAS_norm_Top1j_lvlv2012")
correlation.RenameParameter("lvlv2012", "ATLAS_norm_Top2j", "scale_ATLAS_norm_Top2j_lvlv2012")
correlation.RenameParameter("lvlv2012", "ATLAS_norm_WW0j", "scale_ATLAS_norm_WW0j_lvlv2012")
correlation.RenameParameter("lvlv2012", "ATLAS_norm_WW1j", "scale_ATLAS_norm_WW1j_lvlv2012")
correlation.RenameParameter("lvlv2012", "PM_EFF_f_recoil_DY0j", "scale_PM_EFF_f_recoil_DY0j_lvlv2012")
correlation.RenameParameter("lvlv2012", "PM_EFF_f_recoil_DY1j", "scale_PM_EFF_f_recoil_DY1j_lvlv2012")
correlation.RenameParameter("lvlv2012", "PM_EFF_f_recoil_NDY_SR0j", "scale_PM_EFF_f_recoil_NDY_SR0j_lvlv2012")
correlation.RenameParameter("lvlv2012", "PM_EFF_f_recoil_NDY_SR1j", "scale_PM_EFF_f_recoil_NDY_SR1j_lvlv2012")
correlation.RenameParameter("lvlv2012", "PM_EFF_f_recoil_NDY_ZP0j", "scale_PM_EFF_f_recoil_NDY_ZP0j_lvlv2012")
correlation.RenameParameter("lvlv2012", "PM_EFF_f_recoil_NDY_ZP1j", "scale_PM_EFF_f_recoil_NDY_ZP1j_lvlv2012")

# Add explicitly correlations between different measurements.
# Alternatively the auto-correlation can be used and only parameters
# that should not be correlated are renamed.
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_BR_VV,lvlv2012::alpha_ATLAS_BR_VV", "ATLAS_BR_VV")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_BTag_BEFF,lvlv2012::alpha_ATLAS_BTag_BEFF", "ATLAS_BTag_BEFF")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_BTag_CEFF,lvlv2011::alpha_ATLAS_CTag_CEFF", "ATLAS_BTag_CEFF_2011")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_BTag_LEFF,lvlv2011::alpha_ATLAS_LTag_LEFF,lvlv2012::alpha_ATLAS_BTag_LEFF", "ATLAS_BTag_LEFF")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_EL_EFF,lvlv2012::alpha_ATLAS_EL_EFF", "ATLAS_EL_EFF")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_EL_ESCALE,lvlv2012::alpha_ATLAS_EL_ESCALE", "ATLAS_EL_ESCALE")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_EL_RES,lvlv2012::alpha_ATLAS_EL_RES", "ATLAS_EL_RES")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_EW_MODEL_VV_HWW,lvlv2012::alpha_ATLAS_EW_MODEL_VV_HWW", "ATLAS_EW_MODEL_VV_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_EW_MODEL_Z_HWW,lvlv2012::alpha_ATLAS_EW_MODEL_Z_HWW", "ATLAS_EW_MODEL_Z_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_Higgs_UEPS,lvlv2012::alpha_ATLAS_Higgs_UEPS", "ATLAS_UEPS")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_ISO,lvlv2012::alpha_ATLAS_ISO", "ATLAS_ISO")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_JER,lvlv2012::alpha_ATLAS_JER", "ATLAS_JER")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_JES_2011_Detector1,lvlv2012::alpha_ATLAS_JES_2012_Detector1", "ATLAS_JES_1112_Detector1")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_JES_2011_Modelling1,lvlv2012::alpha_ATLAS_JES_2012_Modelling1", "ATLAS_JES_1112_Modelling1")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_JES_BJET,lvlv2012::alpha_ATLAS_JES_BJET", "ATLAS_JES_Flavb")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_JES_CLOSEBY,lvlv2012::alpha_ATLAS_JES_CLOSEBY,", "ATLAS_JES_Closeby")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_JES_Eta_Modelling,lvlv2012::alpha_ATLAS_JES_Eta_Modelling", "ATLAS_JES_Eta_Modelling")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_JES_FlavComp_HWW_other,lvlv2012::alpha_ATLAS_JES_FlavComp_HWW_other", "ATLAS_JES_FlavComp_HWW_other")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_JES_FlavComp_HWW_tt,lvlv2012::alpha_ATLAS_JES_FlavComp_HWW_tt", "ATLAS_JES_FlavComp_HWW_tt")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_JES_FlavComp_HWW_WW,lvlv2012::alpha_ATLAS_JES_FlavComp_HWW_WW", "ATLAS_JES_FlavComp_HWW_WW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_JES_FlavResp,lvlv2012::alpha_ATLAS_JES_FlavResp", "ATLAS_JES_FlavResp")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_JES_HighPt,lvlv2012::alpha_ATLAS_JES_HighPt", "ATLAS_JES_HighPt")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_JES_MU,lvlv2012::alpha_ATLAS_JES_MU", "ATLAS_JES_MU")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_JES_NPV,lvlv2012::alpha_ATLAS_JES_NPV", "ATLAS_JES_NPV")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_MU_EFF,lvlv2012::alpha_ATLAS_MU_EFF", "ATLAS_MU_EFF")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_MU_ESCALE,lvlv2012::alpha_ATLAS_MU_ESCALE", "ATLAS_MU_ESCALE")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_MU_ID_RES,lvlv2012::alpha_ATLAS_MU_ID_RES", "ATLAS_MU_ID_RES")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_MU_MS_RES,lvlv2012::alpha_ATLAS_MU_MS_RES", "ATLAS_MU_MS_RES")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_TOP_SCALEF_NONTOP_0j_HWW,lvlv2012::alpha_ATLAS_TOP_SCALEF_NONTOP_0j_HWW", "ATLAS_TOP_SCALEF_NONTOP_0j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_TOP_SCALEF_STATS_0j_HWW,lvlv2012::alpha_ATLAS_TOP_SCALEF_STATS_0j_HWW", "ATLAS_TOP_SCALEF_STATS_0j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_TOP_SCALEF_THEO_0j_HWW,lvlv2012::alpha_ATLAS_TOP_SCALEF_THEO_0j_HWW", "ATLAS_TOP_SCALEF_THEO_0j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_TOP_SCALEF_TOPO_CR_0j_HWW,lvlv2012::alpha_ATLAS_TOP_SCALEF_TOPO_CR_0j_HWW", "ATLAS_TOP_SCALEF_TOPO_CR_0j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_TOP_SCALEF_TOPO_SR_0j_HWW,lvlv2012::alpha_ATLAS_TOP_SCALEF_TOPO_SR_0j_HWW", "ATLAS_TOP_SCALEF_TOPO_SR_0j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_TOP_THEO_1j_HWW,lvlv2012::alpha_ATLAS_TOP_THEO_1j_HWW", "ATLAS_TOP_THEO_1j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_TOP_THEO_2j_HWW,lvlv2012::alpha_ATLAS_TOP_THEO_2j_HWW", "ATLAS_TOP_THEO_2j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_TRIGGER_HWW,lvlv2012::alpha_ATLAS_TRIGGER_HWW", "ATLAS_TRIGGER_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_UE,lvlv2012::alpha_ATLAS_UE", "ATLAS_UE")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_WW_MODEL_HWW,lvlv2012::alpha_ATLAS_WW_MODEL_HWW", "ATLAS_WW_MODEL_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_WW_MTSHAPE,lvlv2012::alpha_ATLAS_WW_MTSHAPE", "ATLAS_WW_MTSHAPE_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_WW_UEPS_HWW,lvlv2012::alpha_ATLAS_WW_UEPS_HWW", "ATLAS_WW_UEPS_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_Z_LEPLEP_METCF_STAT_HWW,lvlv2012::alpha_ATLAS_Z_LEPLEP_METCF_STAT_HWW", "ATLAS_Z_LEPLEP_METCF_STAT_HWW_2011")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_Z_TAUTAU_NORM_STAT_HWW,lvlv2012::alpha_ATLAS_Z_TAUTAU_NORM_STAT_HWW", "ATLAS_Z_TAUTAU_NORM_STAT_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_Z_TAUTAU_NORM_SYS_HWW,lvlv2012::alpha_ATLAS_Z_TAUTAU_NORM_SYS_HWW", "ATLAS_Z_TAUTAU_NORM_SYS_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_Z_TAUTAU_VBFCF_STAT_HWW,lvlv2012::alpha_ATLAS_Z_TAUTAU_VBFCF_STAT_HWW", "ATLAS_Z_TAUTAU_VBFCF_STAT_HWW_2011")
correlation.CorrelateParameter("lvlv2011::alpha_FakeRate_EL_HWW,lvlv2012::alpha_FakeRate_EL_HWW", "ATLAS_FakeRate_EL_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_FakeRate_MU_HWW,lvlv2012::alpha_FakeRate_MU_HWW", "ATLAS_FakeRate_MU_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_pdf_gg,lvlv2012::alpha_pdf_gg", "pdf_Bkg_gg")
correlation.CorrelateParameter("lvlv2011::alpha_pdf_gg_ACCEPT,lvlv2012::alpha_pdf_gg_ACCEPT", "pdf_Bkg_gg_ACCEPT")
correlation.CorrelateParameter("lvlv2011::alpha_pdf_Higgs_gg,lvlv2012::alpha_pdf_Higgs_gg", "pdf_Higgs_ggH")
correlation.CorrelateParameter("lvlv2011::alpha_pdf_Higgs_qq,lvlv2012::alpha_pdf_Higgs_qq", "pdf_Higgs_qqH")
correlation.CorrelateParameter("lvlv2011::alpha_pdf_qq,lvlv2012::alpha_pdf_qq", "pdf_Bkg_qq")
correlation.CorrelateParameter("lvlv2011::alpha_pdf_qq_ACCEPT,lvlv2012::alpha_pdf_qq_ACCEPT", "pdf_Bkg_qq_ACCEPT")
correlation.CorrelateParameter("lvlv2011::alpha_pdf_Wg_ACCEPT_HWW,lvlv2012::alpha_pdf_Wg_ACCEPT_HWW", "pdf_Bkg_Wg_ACCEPT_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_pdf_Wgs_ACCEPT_HWW,lvlv2012::alpha_pdf_Wgs_ACCEPT_HWW", "pdf_Bkg_Wgs_ACCEPT_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_PM_f_recoil_DY_SR0j_HWW,lvlv2012::alpha_PM_f_recoil_DY_SR0j_HWW", "ATLAS_PM_f_recoil_DY_SR0j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_PM_f_recoil_DY_SR1j_HWW,lvlv2012::alpha_PM_f_recoil_DY_SR1j_HWW", "ATLAS_PM_f_recoil_DY_SR1j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_PM_f_recoil_NDY_SR0j_HWW,lvlv2012::alpha_PM_f_recoil_NDY_SR0j_HWW", "ATLAS_PM_f_recoil_NDY_SR0j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_PM_f_recoil_NDY_SR1j_HWW,lvlv2012::alpha_PM_f_recoil_NDY_SR1j_HWW", "ATLAS_PM_f_recoil_NDY_SR1j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_PM_f_recoil_NDY_ZP0j_HWW,lvlv2012::alpha_PM_f_recoil_NDY_ZP0j_HWW", "ATLAS_PM_f_recoil_NDY_ZP0j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_PM_f_recoil_NDY_ZP1j_HWW,lvlv2012::alpha_PM_f_recoil_NDY_ZP1j_HWW", "ATLAS_PM_f_recoil_NDY_ZP1j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_PM_theta_SR0j,lvlv2012::alpha_PM_theta_SR0j", "ATLAS_PM_theta_SR0j")
correlation.CorrelateParameter("lvlv2011::alpha_PM_theta_SR1j,lvlv2012::alpha_PM_theta_SR1j", "ATLAS_PM_theta_SR1j")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_ggH,lvlv2012::alpha_QCDscale_ggH", "QCDscale_Higgs_ggH")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_ggH1in,lvlv2012::alpha_QCDscale_ggH1in", "QCDscale_Higgs_ggH1in")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_ggH2in,lvlv2012::alpha_QCDscale_ggH2in", "QCDscale_Higgs_ggH2in")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_ggH3in,lvlv2012::alpha_QCDscale_ggH3in", "QCDscale_Higgs_ggH3in")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_ggH_ACCEPT,lvlv2012::alpha_QCDscale_ggH_ACCEPT", "QCDscale_Higgs_ggH_ACCEPT")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_qqH,lvlv2012::alpha_QCDscale_qqH", "QCDscale_Higgs_qqH")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_qqH_ACCEPT,lvlv2012::alpha_QCDscale_qqH_ACCEPT", "QCDscale_Higgs_qqH_ACCEPT")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_V,lvlv2012::alpha_QCDscale_V", "QCDscale_Bkg_V")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_VV,lvlv2012::alpha_QCDscale_VV", "QCDscale_Bkg_VV_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_VV2in,lvlv2012::alpha_QCDscale_VV2in", "QCDscale_Bkg_VV2in_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_VV_ACCEPT,lvlv2012::alpha_QCDscale_VV_ACCEPT", "QCDscale_Bkg_VV_ACCEPT_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_Wg_ACCEPT0j_HWW,lvlv2012::alpha_QCDscale_Wg_ACCEPT0j_HWW", "QCDscale_Bkg_Wg_ACCEPT0j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_Wg_ACCEPT1j_HWW,lvlv2012::alpha_QCDscale_Wg_ACCEPT1j_HWW", "QCDscale_Bkg_Wg_ACCEPT1j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_Wgs_ACCEPT0j_HWW,lvlv2012::alpha_QCDscale_Wgs_ACCEPT0j_HWW", "QCDscale_Bkg_Wgs_ACCEPT0j_HWW")
correlation.CorrelateParameter("lvlv2011::alpha_QCDscale_Wgs_ACCEPT1j_HWW,lvlv2012::alpha_QCDscale_Wgs_ACCEPT1j_HWW", "QCDscale_Bkg_Wgs_ACCEPT1j_HWW")

# Use the correlation scheme for the combined measurement.
combined.SetCorrelationScheme(correlation)

# Run the combination. First all measurements are regularised, i.e. the
# structure of the PDF will be unified, parameters and constraint terms renamed
# according to a common convention, etc. Then a new simultaneous PDF and
# dataset is build.
combined.CollectMeasurements()
combined.CombineMeasurements()

# Change the 2012 luminosity to 20.3 fb-1 by pulling the nuisance parameter
# down by 0.5 sigma, don't touch the uncertainty.
# Scale the di-tau and VH(bb) measurements to 125.5 GeV by re-scaling XS and BR
# to account for the required difference.
corrections = ROOT.ParametrisationScheme("corrections")
corrections.AddExpression("ATLAS_LUMI_2012_diboson=LinearVar::ATLAS_LUMI_2012_diboson_linear(ATLAS_LUMI_2012[0.0,-5.0,5.0],1.0,-0.5555)")
corrections.AddExpression("ATLAS_LUMI_2012_dibosonConstraint=Gaussian::ATLAS_LUMI_2012Constraint(ATLAS_LUMI_2012,nom_ATLAS_LUMI_2012[0.0],1.0)")
corrections.AddExpression("nom_ATLAS_LUMI_2012_diboson=nom_ATLAS_LUMI_2012[0.0]")
corrections.AddNewGlobalObservable("nom_ATLAS_LUMI_2012")
corrections.AddNewNuisanceParameters("ATLAS_LUMI_2012")
corrections.AddExpression("mu_XS8_ggF_linear=expr::mu_XS8_ggF_scaling('0.992*@0',mu_XS8_ggF[1.0])")
corrections.AddExpression("mu_XS8_VBF_linear=expr::mu_XS8_VBF_scaling('0.9968*@0',mu_XS8_VBF[1.0])")
corrections.AddExpression("mu_BR_tautau_linear=expr::mu_BR_tautau_scaling('0.987*@0',mu_BR_tautau[1.0])")

# Add common signal strength parameter for all signal samples
signalstrength = ROOT.ParametrisationScheme("signalstrength")
signalstrength.AddExpression("mu[1.0,-10.0,10.0]")
signalstrength.AddExpression("mu_XS7_ggF=expr::mu_XS7_ggF_prime('@0*@1',mu,mu_XS7_ggF)")
signalstrength.AddExpression("mu_XS8_ggF=expr::mu_XS8_ggF_prime('@0*@1',mu,mu_XS8_ggF)")
signalstrength.AddExpression("mu_XS7_VBF=expr::mu_XS7_VBF_prime('@0*@1',mu,mu_XS7_VBF)")
signalstrength.AddExpression("mu_XS8_VBF=expr::mu_XS8_VBF_prime('@0*@1',mu,mu_XS8_VBF)")
signalstrength.AddExpression("mu_XS7_WH=expr::mu_XS7_WH_prime('@0*@1',mu,mu_XS7_WH)")
signalstrength.AddExpression("mu_XS8_WH=expr::mu_XS8_WH_prime('@0*@1',mu,mu_XS8_WH)")
signalstrength.AddExpression("mu_XS7_ZH=expr::mu_XS7_ZH_prime('@0*@1',mu,mu_XS7_ZH)")
signalstrength.AddExpression("mu_XS8_ZH=expr::mu_XS8_ZH_prime('@0*@1',mu,mu_XS8_ZH)")

# Combine the different parametrisations
parametrisation = ROOT.ParametrisationSequence("parametrisation")
parametrisation.AddScheme(corrections)
parametrisation.AddScheme(signalstrength)
parametrisation.SetParametersOfInterest("mu,mu_XS7_ggF,mu_XS8_ggF,mu_XS7_VBF,mu_XS8_VBF,mu_XS7_WH,mu_XS8_WH,mu_XS7_ZH,mu_XS8_ZH,mu_BR_WW,mu_BR_tautau")

# Carry out the re-parametrisation
combined.SetParametrisationSequence(parametrisation)
combined.ParametriseMeasurements()

# Generate Asimov data (NP measured in unconditional fit, generated for mu = 1)
combined.MakeAsimovData(ROOT.kTRUE, ROOT.CombinedMeasurement.ucmles, ROOT.CombinedMeasurement.nominal)

# Save the combined workspace
combined.writeToFile("workspaces/combined.root")

# Print useful information like the correlation scheme, re-namings, etc.
combined.Print()
